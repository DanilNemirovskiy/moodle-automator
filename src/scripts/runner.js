const path = require('path');
const fs = require('fs');
const puppeteer = require('puppeteer');

const isDev = process.env.NODE_ENV !== 'production';
const MOODLE_LOGIN_URL = 'http://moodle.kntu.kr.ua/login/index.php';

const params = {
	selectors: {
		login: {
			page: '#login',
			userName: '#username',
			password: '#password',
			loginBtn: '#loginbtn'
		},
		main: {
			container: '#region-main-box',
		},
		courses: {
			links: '.course_list .title a',
			content: '.course-content'
		},
	},
	delays: {
		courseContentPage: 5000,
		idle: 30 * 1000
	},
};

const user = {
	login: process.env.USER_NAME,
	pass: process.env.PASSWORD,
};

async function processCoursePage(page, href) {
	await page.goto(href);

	await page.waitForSelector(params.selectors.courses.content);

	const screenDir = path.join(process.cwd(), 'screenshots/');

	await fs.promises.mkdir(screenDir, {recursive: true});

	const {searchParams} = new URL(href);
	const file = `${screenDir}/${searchParams.get('id')}.png`;

	console.log(`[screenshot] saved to: ${file}`);

	await page.screenshot({path: file});

	await page.waitFor(params.delays.courseContentPage);
}

async function login(page) {
	await page.goto(MOODLE_LOGIN_URL);

	const {login: loginSelectors, main: mainSelectors} = params.selectors;

	await page.waitForSelector(loginSelectors.page);

	await page.waitForSelector(loginSelectors.userName);

	await page.type(loginSelectors.userName, user.login);
	await page.type(loginSelectors.password, user.pass);
	await page.click(loginSelectors.loginBtn);

	await page.waitForSelector(mainSelectors.container);

	console.log('[navigated] to main');
}

(async () => {
	const browser = await puppeteer.launch({
		headless: !isDev,
		devtools: isDev,
	});
	const page = await browser.newPage();

	await page.setViewport({width: 1600, height: 1200});

	await login(page);

	const links = await page.evaluate((params) => {
		return Array.from(document.querySelectorAll(params.selectors.courses.links)).map(a => a.href).filter(Boolean);
	}, params);

	for (const link of links) {
		await processCoursePage(page, link);
	}

	// just stay on the site some time
	await page.waitFor(params.delays.idle);

	await browser.close();
})()
	.catch(console.error)
	.catch((err) => {
		console.error(err);

		process.exit(-1);
	});
